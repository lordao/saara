import os.path
import sqlite3

DATABASE_NAME = "saara.db"

def connect():
    database_exists = os.path.exists(DATABASE_NAME)
    conn = sqlite3.connect(DATABASE_NAME)
    if not database_exists:
        populate(conn)
    return conn

def populate(conn):
    with open("schema.sql", "r") as file:
        query = ""
        for line in file:
            if line == "\n":
                print(line)
            line = line.strip()
            query += line
            if line == "":
                print(query)
                conn.execute(query)
                query = ""
    conn.commit()

def list_items(conn, table, upper):
    loc = []
    result = conn.execute("select nome from {};".format(table))
    if upper:
        loc = [nome[0].upper() for nome in result]
    else:
        loc = [nome[0] for nome in result]
    return loc

def get_municipios(conn, upper=False):
    return list_items(conn, "municipio", upper)

def get_bairros(conn, upper=False):
    return list_items(conn, "bairro", upper)

def get_locations(upper=False):
    conn = connect()
    municipios = get_municipios(conn, upper)
    bairros = get_bairros(conn, upper)
    conn.close()
    return [nome for nome in municipios + bairros]

