#!/usr/bin/env python3
import feedparser
import re

import database

def get_feed():
    with open(".feed_url", "r") as file:
        rss_url = file.read().strip()
    deso_rss = feedparser.parse(rss_url)
    return deso_rss['entries']

def find_locations(locations, entry):
    found = set()
    for text in entry.values():
        if not hasattr(text, "upper"):
            continue
        text = text.upper()
        for location in locations:
            if location in text:
                found.add(location)
    return found

def find_dates(text):
    date = re.compile(r"(\d{1,2}\/\d{2}\/\d{2,4})")
    return date.findall(text)

def main():
    feed = get_feed()
    locations = set(database.get_locations(upper=True))
    for entry in feed:
        found = find_locations(locations, entry)
        if found:
            print("{}:\n  {}".format(entry["link"], found))
            dates = find_dates(entry["description"])
            if len(dates) == 1:
                print("{}\n".format(dates[0]))
            elif len(dates) == 2:
                print("{} - {}\n".format(dates[0], dates[1]))
        else:
            print("None found: {}".format(entry["link"]))

if __name__ == "__main__":
    main()
